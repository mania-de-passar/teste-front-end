# Teste Front End Mobile React Native - OMO Lavanderia

O objetivo do teste é criar uma aplicação em React Native que consuma a API do Github, a fim de listar os repositórios de determinado usuário/organização.

Cada repositório deve ser exibido em uma lista, com nome, thumbnail (caso tenha), quantidade de stars e forks.

Com a API que fornecemos, você deverá permitir que o usuário adicione um repositório aos favoritos, além de poder lista-los e excluir determinado repositório.

O endpoint do GitHub para listagem de repositórios é `https://api.github.com/users/:userOrOrganization/repos`.

## Entrega do teste

Crie um repositório (GitLab ou GitHub) e envie o seu projeto pronto.

### Observações:

- Para a UI do React Native, não use nenhum framework pronto. Utilize o StyleSheet padrão do React Native.
- Caso queira usar o projeto em TypeScript, fique à vontade.
- Não é obrigatório uso de Redux na aplicação, mas caso queira usar, será um diferencial.
- Não use Expo.
- Use o [axios](https://github.com/axios/axios) para chamadas HTTP


## Plus (Diferencial)

Essa etapa não é obrigatória, mas será considerado um diferencial a implementação dos favoritos usando a API em JSON localizada nesse projeto.

## Como executar a API

Faça o clone/download deste repositório, instale o [json-server](https://github.com/typicode/json-server) globalmente, e execute `json-server api.json`. A mesma rodará no endereço `http://localhost:3000`.

## Rotas

Esta API contém as seguintes rotas:

- `GET /favorites` : lista favoritos
- `GET /favorites/:id` : busca favorito por ID
- `POST /favorites` : cadastra novo favorito
- `DELETE /favorites/:id` : exclui favorito por ID
